# Facebook Filter

The module provides a text filter to automatically convert Facebook #hashtags
into links.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/fb_filter)

To submit bug reports and feature suggestions, or track changes
[issue queue](https://www.drupal.org/project/issues/fb_filter)


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

This module does not have configuration.


## Maintainers

- Tamás Pintér - [mr.york](https://www.drupal.org/u/mryork)
- Bálint Nagy - [nagy.balint](https://www.drupal.org/u/nagybalint)
