<?php

namespace Drupal\fb_filter\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to convert Facebook #hashtags into links.
 *
 * @Filter(
 *   id = "fb_filter",
 *   title = @Translation("Facebook filter"),
 *   description = @Translation("Convert Facebook #hashtags into links"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   settings = {
 *     "link_hashtags_target" = "none",
 *   }
 * )
 */
class FbFilter extends FilterBase {

  public const SITE = 'https://www.facebook.com';

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['link_hashtags_target'] = [
      '#type' => 'select',
      '#title' => $this->t('The #hashtags link open in new tab'),
      '#options' => [
        'none' => $this->t('No'),
        '_blank' => $this->t('Yes'),
      ],
      '#default_value' => $this->settings['link_hashtags_target'] ?? 'none',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('Facebook #hashtags turn into links automatically.');
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $target = '';
    if ('_blank' === $this->settings['link_hashtags_target']) {
      $target = ' target="_blank"';
    }

    $text = preg_replace('/(^|\s)#(\w*[\p{M}\p{L}]+[\p{M}\p{L}]*)/u', '\1<a class="facebook-hashtag"' . $target . ' href="' . self::SITE . '/hashtag/\2">#\2</a>', (string) $text);

    return new FilterProcessResult($text);
  }

}
