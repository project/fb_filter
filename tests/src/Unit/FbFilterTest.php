<?php

namespace Drupal\Tests\fb_filter\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\fb_filter\Plugin\Filter\FbFilter;

/**
 * Tests Facebook Filter functions.
 *
 * @coversDefaultClass \Drupal\fb_filter\Plugin\Filter\FbFilter
 * @group FacebookFilter
 */
class FbFilterTest extends UnitTestCase {

  /**
   * Facebook filter.
   *
   * @var \Drupal\fb_filter\Plugin\Filter\FbFilter
   */
  protected $filter;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $configuration['settings'] = [
      'link_hashtags_target' => 'none',
    ];

    $this->filter = new FbFilter($configuration, 'fb_filter', ['provider' => 'test']);
  }

  /**
   * @covers ::process
   *
   * @dataProvider providerFacebookFilterProcessHashtagHashtagPage
   *
   * @param string $input
   *   Input string.
   * @param string $expected
   *   The expected output string.
   */
  public function testFacebookFilterProcessHashtagHashtagPage(string $input, string $expected): void {
    $result = (string) $this->filter->process($input, 'und');
    $this->assertEquals($expected, $result);
  }

  /**
   * Data provider for testFacebookFilterProcessHashtagHashtagPage().
   *
   * @return array
   *   An array of test data.
   */
  public static function providerFacebookFilterProcessHashtagHashtagPage(): array {
    return [
      [
        self::providerMessage(),
        'Drupal says <a class="facebook-hashtag" href="' . FbFilter::SITE . '/hashtag/hello">#hello</a>.',
      ],
    ];
  }

  /**
   * Provides a string with an example Facebook message.
   *
   * @return string
   *   An example Facebook message as a string.
   */
  public static function providerMessage(): string {
    return 'Drupal says #hello.';
  }

}
